<?php
session_start();
require "./class/Autoloader.php";
Autoloader::register();

use magic\Cart;

if (isset($_SESSION['username'])) {
    $update = new Cart();
    $content = array();
    $json = file_get_contents("./data/cards.json");
    $data = json_decode($json);
    foreach ($data as $elems) {
        foreach ($_SESSION['cart'] as $id => $valeur) {
            if ($id == $elems->id) {
                $content[$valeur] = $_POST[$valeur];
                $content[$id] = $_POST[$id];
            }
        }
    }
//print_r($content);
    $update->update($content);
    header('Location:./cart.php');
} else {
    header('Location:./login.php');
}
?>