<?php
session_start();
require "./class/Autoloader.php";
Autoloader::register();

use magic\Template;
use magic\Browse;

?>
<html>
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" rel="stylesheet"/>
    <title>Browse | - Magic Store</title>
    <link rel="stylesheet" href="style.css"/>
</head>
<body>
<?php
$page = new Template();
$browse = new Browse();
$page->header();
$browse->generateCards();
$page->footer();
?>
</body>
<footer>

</footer>
</html>
