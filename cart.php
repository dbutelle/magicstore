<?php
session_start();
require "./class/Autoloader.php";
Autoloader::register();

use magic\Template;
use magic\Cart;

?>

<html>
<head>

    <title>Cart | - Magic Store</title>

</head>

<body>
<?php
if (isset($_SESSION['username'])) {

    $page = new Template();
    if (isset($_SESSION['cart'])) {
        $cart = new Cart();
        $page->header();
        $cart->render();
        $page->footer();
    } else {
        $_SESSION['cart'] = array();
        $cart = new Cart();
        $page->header();
        $cart->render();
        $page->footer();
    }
} else {
    header('Location:./login.php');
}
?>
</body>

</html>