<?php

namespace magic;
class Browse
{

    public function __construct()
    {
        $json = file_get_contents("./data/cards.json");
        $this->data = json_decode($json);
    }

    public function generateCards(): void
    {

        ?>
        <div id="main-content">
            <header style="margin:15px">
                <div>Magic Cards</div>
            </header>
            <form class="browser">
                <div class="browser-content-wrapper">
                    <div class="browser-content">
                        <?php

                        foreach ($this->data as $donnees) {
                            $img = $donnees->image_uris->normal;
                            echo "<div class='magic-card'>
                                <img src='$img'/>
                                <legend style='justify-content: center'>
                                    <label for='$donnees->id '>$donnees->name</label>
                                </legend>
                            </div>";
                        }

                        ?>

                    </div>
                </div>
            </form>
        </div>


        <?php


    }
}