<?php


namespace magic;


class Cart
{
    public array $content = array();

    public function __construct()
    {
        $this->content = $_SESSION['cart'];
    }

    public function add(array $selection): void
    {
        foreach ($selection as $id => $valeur) {
            if (array_key_exists($id, $this->content)) {
                $this->content[$valeur]++;
            } else {
                $this->content[] = $id;
                $this->content[$valeur]++;
            }
        }
        foreach ($this->content as $id => $valeur) {
            $_SESSION['cart'][$id] = $this->content[$id];
            $_SESSION['cart'][$valeur] = $this->content[$valeur];
        }
    }

    public function render(): void
    {

        ?>
        <div id="main-content">
        <form class='browser' method='post' action='update_cart.php' autocomplete='off'>
        <header style='flex-direction: column;justify-content: center'>
            <h1 style='padding: 5px'>
                CART : <span id='num-cards'>0</span>$
            </h1>
            <button type="submit" class="btn btn-danger" style="width:100%;margin:0px;visibility: hidden;"
                    id="update-cart">
                UPDATE CART
            </button>
        </header>
        <?php
        if (isset($_SESSION['cart'])) {

            ?>
            <div class='browser-content-wrapper'>
                <div class='browser-content'>
                    <?php
                    $json = file_get_contents("./data/cards.json");
                    $data = json_decode($json);
                    foreach ($_SESSION['cart'] as $id => $valeurs) {

                        foreach ($data as $donnees) {
                            if ($id == $donnees->id) {
                                $img = $donnees->image_uris->normal;
                                ?>
                                <div class='magic-card selected'>
                                <img src='<?php echo $img ?>'>
                                <legend>
                                    <label for='<?php echo $donnees->id ?>'><?php echo $donnees->name ?></label>
                                    <input type='number' id='<?php echo $donnees->id ?>' value="<?php echo $valeurs; ?>"
                                           name='<?php echo $donnees->id ?>'/>
                                </legend>
                                </div>
                                <?php
                            }
                        }
                    }


                    ?>
                </div>
            </div>
            </form>
            <script>
                let inputs = undefined
                let numCards = undefined
                let updateButton = undefined
                document.addEventListener('DOMContentLoaded', function () {
                    numCards = document.getElementById('num-cards')
                    updateButton = document.getElementById('update-cart')
                    inputs = document.getElementsByTagName('input')
                    for (input of inputs) {
                        input.addEventListener('change', function () {
                            if (this.value !== this.defaultValue) {
                                this.parentElement.parentElement.classList.remove('selected')
                                this.parentElement.parentElement.classList.add('edited')
                            } else {
                                this.parentElement.parentElement.classList.remove('edited')
                                this.parentElement.parentElement.classList.add('selected')
                            }
                            countItems()
                            styleButton()
                        })
                    }
                    countItems()
                    styleButton()
                })

                function countItems() {
                    const price = 25
                    let count = 0
                    for (input of inputs) {
                        let val = parseInt(input.value)
                        val = val >= 0 ? val : 0
                        count += val
                    }
                    numCards.innerHTML = count * price / 100
                }

                function styleButton() {
                    let edited = document.getElementsByClassName('edited')
                    updateButton.style.visibility = (edited.length != 0) ? 'visible' : 'hidden'
                }

            </script>
            </div>
            <?php
        }


    }

    public function update(array $cart): void
    {

        foreach ($cart as $id => $value) {
            if ($value == 0) {
                unset($_SESSION['cart'][$id]);
            } else {
                $_SESSION['cart'][$id] = $value;
            }
        }
    }
}

?>