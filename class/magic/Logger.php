<?php

namespace magic;
class Logger
{
    public function generateLoginForm(string $action): void
    {
        echo "
    
    <form method='post' action=$action class='card' id='login-form' >
        <legend style='text-align :center;'>Please login</legend>
        <div class='form-group' data-children-count=''>
        <input type='text' name='username' placeholder='username'>
        <input type='password' name='password' placeholder='password'>
        </div>
        
        <input type='submit' class='btn btn-dark' value='login' name='login' id='login'>
    </form>

";

    }

    public function log(string $username, string $password): array
    {
        if ($username == 'gandalf' && $password == 'youshallnotpass') {

            $granted = true;
            $nick = $username;
        } else {
            $granted = false;
            $nick = null;
        }
        if (empty($username)) {
            $error = 'username is empty';
            echo "<div class='magic-card' id='error'> $error </div>";
        } elseif (empty($password)) {
            $error = 'password is empty';
            echo "<div class='magic-card' id='error'> $error </div>";
        } else {
            if (!$granted) {
                $error = 'authentication failed';
                echo "<div class='magic-card' id='error'> $error </div>";
            } else {
                $error = '';
            }
        }
        return [$granted, $nick, $error];
    }
}

?>