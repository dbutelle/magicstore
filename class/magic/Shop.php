<?php


namespace magic;


class Shop
{
    public function __construct()
    {
        $json = file_get_contents("./data/cards.json");
        $this->data = json_decode($json);
    }

    public function generateShop(): void
    {

        ?>
        <div id="main-content">
            <form class="browser" action="./add_to_cart.php" method="POST" autocomplete="off">
                <header style="margin:15px">
                    <div style="padding: 5px">SELECTION :
                        <span id="num-cards">none</span>
                    </div>
                    <button type="submit" class="btn btn-dark" id="add-to-cart" name="submit">Add to Cart</button>
                </header>

                <div class="browser-content-wrapper">
                    <div class="browser-content">
                        <?php

                        foreach ($this->data as $donnees) {
                            $img = $donnees->image_uris->normal;
                            echo "<div class='magic-card'>
                                <img src='$img'/>
                                <legend style='justify-content: center'>
                                    <label for='$donnees->id '>$donnees->name</label>
                                    <input class='form-check-input' type='checkbox' name='selection[]' value='$donnees->id' id='$donnees->id'/>
                                </legend>
                            </div>";
                        }

                        ?>

                    </div>
                </div>
            </form>
            <script>
                let number = undefined
                let count = 0
                let addToCartBtn = undefined
                document.addEventListener('DOMContentLoaded', function () {
                    number = document.getElementById('num-cards');
                    addToCartBtn = document.getElementById('add-to-cart')
                    addToCartBtn.disabled = true
                    let checks = document.querySelectorAll("input[type='checkbox']")
                    for (c of checks) {
                        c.addEventListener('change', function (e) {
                            if (this.checked) {
                                this.parentElement.parentElement.classList.add('selected')
                            } else {
                                this.parentElement.parentElement.classList.remove('selected')
                            }
                            count_selection()
                        })
                        c.parentElement.parentElement.addEventListener('click', function () {
                            let input = this.querySelector("input[type='checkbox']");
                            console.log(input)
                            input.checked = !input.checked;
                            if (input.checked) {
                                this.classList.add('selected')
                            } else {
                                this.classList.remove('selected')
                            }
                            count_selection()
                        })
                    }
                    count_selection()
                })

                function count_selection() {
                    let selected = document.querySelectorAll("input[type='checkbox']:checked")

                    if (selected.length == 0) {
                        addToCartBtn.disabled = true
                        number.innerHTML = 'none'
                    } else {
                        addToCartBtn.disabled = false
                        number.innerHTML = selected.length
                        selected.forEach(input => {
                            console.log(input.parentElement.parentElement)
                        })
                    }
                }
            </script>
        </div>


        <?php


    }
}