<?php
session_start();
require "./class/Autoloader.php";
Autoloader::register();

use magic\Template;
use magic\Logger;

?>


<html>
<head>
    <title>Login | - Magic Store</title>

</head>

<body>
<?php

$page = new Template();
$login = new Logger();
$page->header();
?>
<div id="main-content">
    <?php
    if (!empty($_POST)) {

        $username = htmlspecialchars(trim($_POST['username']));
        $password = htmlspecialchars(trim($_POST['password']));
        $array = $login->log($username, $password);
        if ($array[0] == true) {

            $_SESSION['username'] = $username;
            header("Location:index.php");
        } else {

            $login->generateLoginForm('login.php');
        }
    } else {
        $login->generateLoginForm('login.php');
    }

    ?>
</div>
<?php
$page->footer();
?>
</body>

</html>