<?php
session_start();
require "./class/Autoloader.php";
Autoloader::register();

use magic\Cart;

if (isset($_SESSION['username'])) {

    if (isset($_POST['submit'])) {
        if (isset($_SESSION['cart'])) {
            $content = $_POST['selection'];
            $cart = new Cart();
            $cart->add($content);
            header('Location:cart.php');
        } else {
            $_SESSION['cart'] = array();
            $content = $_POST['selection'];
            $cart = new Cart();
            $cart->add($content);
            header('Location:cart.php');
        }


    }
} else {
    header('Location:./login.php');
}
?>
