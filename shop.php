<?php
session_start();
require "./class/Autoloader.php";
Autoloader::register();

use magic\Template;
use magic\Shop;

?>

<html>
<head>
    <title>Shop | - Magic Store</title>

</head>

<body>
<?php
if (isset($_SESSION['username'])) {


    $page = new Template();
    $shop = new Shop();
    $page->header();
    $shop->generateShop();
    $page->footer();
} else {
    header('Location:./login.php');
}
?>
</body>

</html>
