<html>
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"/>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0"
            crossorigin="anonymous"></script>
    <link rel="stylesheet" href="style.css"/>
</head>
<header>
    <nav class="navbar navbar-dark bg-dark">
        <div class="nav-content">
            <a href="./index.php" class="navbar-brand">
                <div style="width: 0.5em;"></div>
                <h1>Magic Store</h1>
            </a>
            <?php
            if (isset($_SESSION['username'])) {
                ?>
                <a role="button" class="btn btn-dark" href="shop.php">Shop</a>
                <?php
            } else {
                ?>
                <a role="button" class="btn btn-dark" href="browse.php">Browse</a>
                <?php
            }


            ?>

            <div style="flex: 1"></div>
            <?php
            if (isset($_SESSION['username'])){
            ?>
            <div class="btn-group" role="group">
                <a class="btn btn-dark" id='cart' role="button" href="./cart.php">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                         class="bi bi-bag-fill" viewBox="0 0 16 16">
                        <path d="M8 1a2.5 2.5 0 0 1 2.5 2.5V4h-5v-.5A2.5 2.5 0 0 1 8 1zm3.5 3v-.5a3.5 3.5 0 1 0-7 0V4H1v10a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V4h-3.5z"/>
                    </svg>
                </a>
                <div class="dropdown">
                    <button class="btn btn-dark  dropdown-toggle" role="button" data-bs-toggle="dropdown">
                        <?php echo $_SESSION['username']; ?>
                        <span class="caret"></span>
                    </button>
                    <div class="dropdown-menu dropdown-menu-dark dropdown-menu-end">
                        <li><a class="dropdown-item" href="./logout.php">Logout</a></li>
                    </div>
                </div>
                <?php
                } else {
                    ?>
                    <a class="btn btn-dark" role="button" href="./login.php">Login</a>
                    <?php
                }


                ?>

            </div>
        </div>

    </nav>
</header>
</html>
